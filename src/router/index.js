import Vue from 'vue'
import Router from 'vue-router'
import mainPage from '@/components/mainPage'
import status from '@/components/status'
import details from '@/components/details'
import express from '@/components/express'
import schedule from '@/components/schedule'
import dmpi from '@/components/dmpi'
import checkIn from '@/components/checkIn'
import survey from '@/components/survey'
import search from '@/components/search'
import checkout from '@/components/checkout'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'mainPage',
      component: mainPage
    },
    {
      path: '/search',
      name: 'search',
      component: search
    },
    {
      path: '/status',
      name: 'status',
      component: status
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: checkout
    },
    {
      path: '/check-in',
      name: 'checkIn',
      component: checkIn
    },
    {
      path: '/dmpi',
      name: 'dmpi',
      component: dmpi
    },
    {
      path: '/details',
      name: 'details',
      component: details
    },
    {
      path: '/express',
      name: 'express',
      component: express
    },
    {
      path: '/schedule',
      name: 'schedule',
      component: schedule
    },
    {
      path: '/survey',
      name: 'survey',
      component: survey
    },
  ]
})
